class RPNCalculator

  def initialize
    @stack = []
  end

  def push(num)
    @stack << num
  end

  def value
    @stack[-1]
  end

  def plus
    self.error_msg
    num1 = @stack.pop
    num2 = @stack.pop
    @stack << num1 + num2
  end

  def minus
    self.error_msg
    second_num = @stack.pop
    first_num = @stack.pop
    @stack << first_num - second_num
  end

  def divide
    self.error_msg
    divisor = @stack.pop
    dividend = @stack.pop
    @stack << dividend / divisor.to_f
  end

  def times
    self.error_msg
    num1 = @stack.pop
    num2 = @stack.pop
    @stack << num1 * num2
  end

  def error_msg
    raise 'calculator is empty' if @stack.length <= 1
  end

  def tokens(string)
    digits = '0123456789'
    operators = '+-*/'

    string.split.map do |token|
      if digits.include?(token)
        token.to_i
      elsif operators.include?(token)
        token.to_sym
      end
    end
  end

  def evaluate(string)
    expression = tokens(string)
    expression.each do |token|
      case token
      when Integer
        push(token)
      when :+
        plus
      when :-
        minus
      when :*
        times
      when :/
        divide
      end
    end

    @stack[-1]
  end

end
